from collections import Counter
import pandas as pd
import time
import datetime
import os


def parseCategory(dataframe, name, category):
    df = dataframe
    columns = list(df.columns)
    if category in columns:
        columns.remove('Time')
        columns.remove(category)

        return df.drop(columns, axis=1).rename(columns={category: name})
    else:
        raise


def fixDateTime(dataframe):
    """
    Takes a dataframe with string time in the following format 'Sun Oct 22 18:32:52'
    Changes string time to datetime object
    Rounds time to the nearest half hour
    Returns dataframe with a fixed Time column
    """

    df = dataframe
    date = pd.to_datetime(df['Time'], format='%a %b %d %H:%M:%S %Y')
    date = date.apply(lambda dt: datetime.datetime(
        dt.year, dt.month, dt.day, dt.hour, 30 * (dt.minute // 30)))
    df['Time'] = date
    return df


def getDataDirList():
    return os.listdir('data')


def parseNameFromFileName(csv_file):
    return csv_file.split('.')[0]


def getDataFrame(csv_file):
    return pd.read_csv('data/{}').format(csv_file)


def mergeDataFrameByColumn(min_periods=241, column='Followers'):
    mergedFrame = pd.DataFrame()
    data_dir = getDataDirList()
    for count, file in enumerate(data_dir):
        print('{0} of {1}'.format(count, len(data_dir)))
        coin_name = parseNameFromFileName(file)

        df = pd.read_csv('data/{}'.format(file))
        minimum_index = len(df) - min_periods
        if len(df) < min_periods:
            print('skipping {} not enough data.'.format(coin_name))
            continue
        elif df['Followers'][minimum_index] == 0:
            print('skipping {} not enough data.'.format(coin_name))
            continue

        df['Followers'].replace(to_replace=0, method='ffill', inplace=True)

        df = fixDateTime(df)

        parsedFrame = parseCategory(df, coin_name, column)

        if count == 0:
            mergedFrame = parsedFrame
        else:
            mergedFrame = mergedFrame.merge(parsedFrame, on='Time')
    return mergedFrame


def createPickle(dataFrame, column='Followers'):

    folder_name = 'pickledframes/'
    _path = str(folder_name + '{}.pickle'.format(column))
    print(dataFrame)

    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    dataFrame.to_pickle(_path)
    print('{} pickle created.'.format(_path))


def getPickle(column='Followers'):
    folder_name = 'pickledframes/'
    _path = str(folder_name + '{}.pickle'.format(column))
    df = pd.read_pickle(_path)
    df.set_index('Time', inplace=True)
    print('Getting DataPickle Created: {}'.format(
        time.ctime(os.path.getctime(_path))))
    return df


def getPctChangeForNPeriods(dataFrame, n_periods, lenOfList):

    periodsDict = {'H': 'Last Hour',
                   '6H': 'Last 6 Hours',
                   '12H': 'Last 12 Hours',
                   'D': 'Last 24 Hours',
                   '3D': 'Last 3 Days',
                   '5D': 'Last 5 Days'}
    resampled_pct = dataFrame.resample(n_periods).mean().pct_change()

    row = resampled_pct[-1:].reset_index().drop('Time', axis=1)
    row = row.melt(var_name='coin')
    row = row.sort_values('value').dropna().rename(
        columns={'value': 'percent_change'})
    print('Highest percent change of new followers in the {}'.format(
        periodsDict[n_periods]))
    row['percent_change'] = row['percent_change'] * 100.0
    row['percent_change'] = row['percent_change'].astype(float)
    row = row.tail(lenOfList)[::-1].reset_index(drop=True)
    return row


def createFollowerPickle():
    theFrame = mergeDataFrameByColumn()
    createPickle(theFrame)


def filterByMin(dataFrame, min_number=10000):
    print('\n\n\n\n', '* * * * *Filtered by minimum {} followers.* * * * *'.format(min_number))
    latest = dataFrame[-1:]
    bools = (latest > min_number).any(axis=0)
    return list(latest.loc[:, bools].columns)


def getCoinListOnFilteredExchanges():
    folder = 'tickers'
    listOfFileNamesFromFolder = os.listdir(folder)

    exchangeFilterList = []
    for file in listOfFileNamesFromFolder:
        print(file.split('.')[0], 'Exchange added')
        exchangeDataFrame = pd.read_csv('{}/{}'.format(folder, file))
        exchangeFilterSet = set(exchangeFilterList +
                                list(exchangeDataFrame['symbol']))
        exchangeFilterList = list(exchangeFilterSet)

    exchangeFilterList.sort()
    return exchangeFilterList


def filterByExchangeList(dataFrame, dataList):

    columns = list(dataFrame.columns)

    newcol = []
    for col in columns:
        if col in dataList:
            newcol.append(col)
            continue

    return dataFrame[newcol]


def main():
    top_list = []
    exchangeList = getCoinListOnFilteredExchanges()
    df = getPickle()

    filteredExchanges = filterByExchangeList(df, exchangeList)
    #filteredExchanges = df

    filtered = filterByMin(filteredExchanges, 500)

    n = 7

    for period in ['H', '6H', '12H', 'D', '3D', '5D']:
        top_filtered_2500 = getPctChangeForNPeriods(df[filtered], period, n)
        top_list += list(top_filtered_2500['coin'])
        print(top_filtered_2500)

    filtered = filterByMin(filteredExchanges, 1000)

    for period in ['H', '6H', '12H', 'D', '3D', '5D']:
        top_filtered_2500 = getPctChangeForNPeriods(df[filtered], period, n)
        top_list += list(top_filtered_2500['coin'])
        print(top_filtered_2500)

    filtered = filterByMin(filteredExchanges, 2500)

    for period in ['H', '6H', '12H', 'D', '3D', '5D']:
        top_filtered_2500 = getPctChangeForNPeriods(df[filtered], period, n)
        top_list += list(top_filtered_2500['coin'])
        print(top_filtered_2500)

    filtered = filterByMin(filteredExchanges, 5000)

    for period in ['H', '6H', '12H', 'D', '3D', '5D']:
        top_filtered_2500 = getPctChangeForNPeriods(df[filtered], period, n)
        top_list += list(top_filtered_2500['coin'])
        print(top_filtered_2500)

    filtered = filterByMin(filteredExchanges, 10000)

    for period in ['H', '6H', '12H', 'D', '3D', '5D']:
        top_filtered_10000 = getPctChangeForNPeriods(df[filtered], period, n)
        top_list += list(top_filtered_10000['coin'])
        print(top_filtered_10000)

    filtered = filterByMin(filteredExchanges, 15000)

    for period in ['H', '6H', '12H', 'D', '3D', '5D']:
        top_filtered_15000 = getPctChangeForNPeriods(df[filtered], period, n)
        top_list += list(top_filtered_10000['coin'])
        print(top_filtered_15000)

    top_list = Counter(top_list)
    print('\n', '* * * * *Most Hits acoss the board. Left to Right.* * * * *')
    print(str(top_list))
    sorted_list = sorted(top_list, key=top_list.get, reverse=True)

    for x in sorted_list:
        print(x)

    print('\n\n*** Margin List ***')
    for x in sorted_list:
        if x in ['ETH', 'XMR', 'XRP', 'STR', 'XLM', 'LTC', 'DASH', 'FCT', 'BTS', 'MAID', 'CLAM', 'DOGE']:
            print(x)

if __name__ == '__main__':
    main()
    pass
# df = mergeDataFrameByColumn()
# createPickle(df)
