# author Kevin McGee
# Oct 2017

import requests
from bs4 import BeautifulSoup


def parseSoup(soup, string):
    try:
        content = soup.find(
            'li', class_='ProfileNav-item ProfileNav-item--' + string)
        span = content.find('span', class_='ProfileNav-value')
        data = span.attrs['data-count']

    except AttributeError:
        data = 0

    return data


def getUser(account):
    """
    Args:
        account: Twitter account name.

    Returns:
        dict {Tweets, Following, Followers, Likes}
    """

    url = str('https://twitter.com/' + account)
    content = requests.get(url)
    soup = BeautifulSoup(content.text, 'html.parser')

    tweets = parseSoup(soup, 'tweets is-active')
    following = parseSoup(soup, 'following')
    followers = parseSoup(soup, 'followers')
    likes = parseSoup(soup, 'favorites')

    return {'Tweets': tweets, 'Following': following, 'Followers': followers, 'Likes': likes}


if __name__ == '__main__':
    status = getUser('TheScaryNature')
    print(status)

