# author Kevin Mcgee
# Oct 2017
import requests
import os
import logging
import shutil

from datetime import datetime
from get_twitter_handles import handlesFileExists, writeTwitterHandleCSV
from twitter_stats import getUser
from time import time
from csv import DictWriter, DictReader

from tweet_analyser import createFollowerPickle

import pandas as pd

FIELDNAMES = ['Time', 'Followers', 'Following', 'Tweets', 'Likes']
DATA_DIR = os.listdir('data')

logging.basicConfig(filename='twitterstat.log', level=logging.INFO)
logger = logging.getLogger('coin-twitter')


def createEmptyCSV():
    df = pd.read_csv('data/BTC.csv')
    df['Followers'] = 0
    df['Following'] = 0
    df['Tweets'] = 0
    df['Likes'] = 0
    df.to_csv('data/EMPTYFILE.csv', index=False)
    logger.info('new EMPTYFILE created')


def getSymbolsFromDataDir():
    data = []
    for x in DATA_DIR:
        if x != 'EMPTYFILE.csv':
            data.append(x.split('.')[0])
    return data


def getDataFromCSV():
    data = {}
    with open('twitter_handles.csv') as file:
        reader = list(DictReader(file))
        for x in reader:
            data[x['symbol']] = x
    return data


def getSymbolsFromCSV(n=None):
    if not handlesFileExists():
        writeTwitterHandleCSV()

    sym = []

    if n == None:
        with open('twitter_handles.csv') as file:
            reader = list(DictReader(file))
            for x in reader:
                sym.append(x['symbol'])
    else:
        with open('twitter_handles.csv') as file:
            reader = list(DictReader(file))
            for x in reader[:n]:
                sym.append(x['symbol'])
    return sym


def getTop500Symbols():
    sym = getSymbolsFromCSV(n=500)
    return sym


def getListOfUniqueSymbols():
    One = getTop500Symbols()
    Two = getSymbolsFromDataDir()
    print(Two)
    return list(set(One + Two))


def parseSymbolFromData(symbols_list, data):
    coins = []
    for symbol in symbols_list:
        try:
            coins.append(data[symbol])
        except KeyError as e:
            logger.info('{} not working.\n {}'.format(symbol, e))

    return coins


def writeToCsv(id, data):
    folder_name = 'data/'
    empty_file = str(folder_name + 'EMPTYFILE.csv')
    file_name = str(folder_name + id + '.csv')

    # if not os.path.exists(empty_file):
    #     createEmptyCSV()

    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    if not os.path.exists(file_name):
        shutil.copy(empty_file, file_name)
        logger.info('{} file being created.'.format(file_name))

    with open(file_name, 'a') as file:
        writer = DictWriter(file, fieldnames=FIELDNAMES)
        data['Time'] = datetime.now().ctime()
        writer.writerow(data)

    logger.info('Data was written to {}'.format(file_name))


def main():
    createEmptyCSV()
    start = time()

    _symbols = getListOfUniqueSymbols()
    _data = getDataFromCSV()
    coins = parseSymbolFromData(_symbols, _data)

    for coin in coins:
        try:
            account = coin['twitter']
            writeToCsv(coin['symbol'], getUser(account))
            logger.debug('{0} twitter data found.'.format(coin['id']))
        except:
            logger.info('{0} Has no twitter handle'.format(coin['id']))
    totalTime = (time() - start)

    logger.info(
        'Twitter scrape complete. It Took {0} seconds. Averageing {1} per coin.'.format(totalTime, (totalTime / len(coins))))

    createFollowerPickle()

    logger.info('Follower Pickle created.')


if __name__ == '__main__':
    main()
