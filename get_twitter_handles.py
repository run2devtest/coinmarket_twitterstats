# author Kevin McGee
# Oct 2017

import requests
import os
import logging
from time import time

from bs4 import BeautifulSoup
from coinmarketcap import Market

from csv import DictWriter as CSVDictWriter

logging.basicConfig(filename='twitterstat.log', level=logging.INFO)
logger = logging.getLogger('coin-twitter')


def getCoinId():
    """
    Gets the list of n_coins from coinmarketcap.
    Returns a list of the coins id name
    """
    coi = Market()
    coins_list = []
    for coin in coi.ticker():
        coins_list.append((coin['symbol'], coin['id']))
    return coins_list


def getTwitterHandle(coin_id):
    """
    Takes a symbol and id of a coin
    Returns a dictionary {symbol, coin_id, twitter handle}
    """
    url = 'https://coinmarketcap.com/currencies/{coin}/#social'
    page = requests.get(url.format(coin=coin_id))
    soup = BeautifulSoup(page.content, 'html.parser')

    social_section = soup.find(id='social')
    twitter_handle = social_section.find('a').text.split('@')[1]
    return twitter_handle


def createTwitterList():
    twitter_list = []
    start = time()
    for coin in getCoinId():
        symbol = coin[0]
        coin_id = coin[1]
        try:
            handle = getTwitterHandle(coin_id)
            # logger.info('{} {} {} found'.format(symbol, coin_id, handle))
            twitter_list.append(
                {'symbol': symbol, 'id': coin_id, 'twitter': handle})
        except AttributeError:
            logger.info('{} {} No Twitter ID Found'.format(symbol, coin_id))
    logger.info(
        'List of all coins created. This took {} minutes'.format((time() - start) // 60))
    return twitter_list


def handlesFileExists():
    return os.path.isfile('twitter_handles.csv')


def writeTwitterHandleCSV():
    filename = 'twitter_handles.csv'
    FIELDNAMES = ['symbol', 'id', 'twitter']

    with open(filename, 'w') as file:
        writer = CSVDictWriter(file, fieldnames=FIELDNAMES)
        writer.writeheader()
        for data in createTwitterList():
            writer.writerow(data)


if __name__ == '__main__':
    print(handlesFileExists())
    writeTwitterHandleCSV()
    print(handlesFileExists())
