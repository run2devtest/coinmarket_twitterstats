from bs4 import BeautifulSoup
from csv import DictWriter

import requests
import os

from coinmarketcap import Market


FIELDNAMES = ['symbol', 'id', 'name', 'rank',
              'last_updated', 'available_supply', 'total_supply', '24h_volume_usd']
FOLDER = 'tickers/'

POLONIEX = {
    'page': 'https://coinmarketcap.com/exchanges/poloniex/', 'name': 'Poloniex'}
BITTREX = {
    'page': 'https://coinmarketcap.com/exchanges/bittrex/', 'name': 'Bittrex'}
BINANCE = {
    'page': 'https://coinmarketcap.com/exchanges/binance/', 'name': 'Binance'}
LIQUI = {
    'page': 'https://coinmarketcap.com/exchanges/liqui/', 'name': 'Liqui'}
ETHERDELTA = {
    'page': 'https://coinmarketcap.com/exchanges/etherdelta/', 'name': 'Etherdelta'}
HITBTC = {
    'page': 'https://coinmarketcap.com/exchanges/hitbtc/', 'name': 'Hitbtc'}
COINEXCHANGE = {
    'page': 'https://coinmarketcap.com/exchanges/coinexchange/', 'name': 'CoinExchage'}

EXCHANGE_PAGES = [POLONIEX, BITTREX, BINANCE, LIQUI, HITBTC, COINEXCHANGE]

# TODO: add comments to function parts.


def exchangeCoinsToCSV(page, folder):
    """
    Pulls data from CoinMCap.
    Parses data.
    Creates a list of coins on the exhange page.
    """
    exchange_name = page['name']
    webpage = page['page']

    coi = Market()
    response = requests.get(webpage)
    soup = BeautifulSoup(response.text, 'html.parser')

    coins = []
    href = soup.find_all('a', target='_blank')
    for x in href:
        line = x.text
        if '/BTC' in line:
            parsed = line.replace('/BTC', '')
            coins.append(parsed)

    if not os.path.exists(folder):
        os.makedirs(folder)
        logger.info('Creating {} folder'.format(folder))

    with open('{0}{1}.csv'.format(folder, exchange_name), 'w') as file:
        writer = DictWriter(file, fieldnames=FIELDNAMES)
        writer.writeheader()

        for ticker in coi.ticker():
            # print(ticker)
            parsed_coins = {}
            if ticker['symbol'] in coins:
                for name in FIELDNAMES:
                    parsed_coins[name] = ticker[name]
                writer.writerow(parsed_coins)
                print('Added {0} to {1}.csv'.format(
                    ticker['symbol'], exchange_name))


def etherDeltaCoinsToCSV(page, folder):
    """
    Pulls data from CoinMCap.
    Parses data.
    Creates a list of coins on the exhange page.
    """
    exchange_name = page['name']
    webpage = page['page']

    coi = Market()
    response = requests.get(webpage)
    soup = BeautifulSoup(response.text, 'html.parser')

    coins = []
    href = soup.find_all('a', target='_blank')
    for x in href:
        line = x.text
        if '/ETH' in line:
            parsed = line.replace('/ETH', '')
            coins.append(parsed)

    if not os.path.exists(folder):
        os.makedirs(folder)
        logger.info('Creating {} folder'.format(folder))

    with open('{0}{1}.csv'.format(folder, exchange_name), 'w') as file:
        writer = DictWriter(file, fieldnames=FIELDNAMES)
        writer.writeheader()

        for ticker in coi.ticker():
            # print(ticker)
            parsed_coins = {}
            if ticker['symbol'] in coins:
                for name in FIELDNAMES:
                    parsed_coins[name] = ticker[name]
                writer.writerow(parsed_coins)
                print('Added {0} to {1}.csv'.format(
                    ticker['symbol'], exchange_name))


def getExchangeTickerInfo():
    """
    Cycles through xchange_pages, pulls data. 
    Creates Exchange.csv
    """
    for page in EXCHANGE_PAGES:
        exchangeCoinsToCSV(page, FOLDER)

    etherDeltaCoinsToCSV(ETHERDELTA, FOLDER)


if __name__ == '__main__':
    getExchangeTickerInfo()
